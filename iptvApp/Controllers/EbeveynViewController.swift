//
//  EbeveynViewController.swift
//  iptvApp
//
//  Created by Arif Doğan on 27.08.2018.
//  Copyright © 2018 Arif Doğan. All rights reserved.
//

import UIKit
class EbeveynViewController: UIViewController  {

    
    fileprivate var savedTrackPath: [Int] = UserDefaults.standard.array(forKey: "lockKey") as? [Int] ?? [Int]()
    fileprivate var timer: Timer?
    
    @IBOutlet var patternLock: OXPatternLock!
    @IBOutlet var labelStatus: UILabel!
    
    @IBOutlet weak var lockImage: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print(self.savedTrackPath)
        
        self.patternLock.delegate = self
        
        self.lockImage.isUserInteractionEnabled = true
        
        let lockReco = UITapGestureRecognizer(target: self, action: #selector(EbeveynViewController.lock))
  
        self.lockImage.addGestureRecognizer(lockReco)
    
    }

    
    @objc func lock(){
        
        UserDefaults.standard.set(true, forKey: "lock")
        UserDefaults.standard.synchronize()
        
        performSegue(withIdentifier: "lockToMenu", sender: self)
    }
    
   
    
    fileprivate func showStatus(message: String) {
        labelStatus.text = message
        timer = Timer.scheduledTimer(withTimeInterval: 3, repeats: false, block: { _ in
            self.labelStatus.text = ""
        })
    }
}

extension EbeveynViewController: OXPatternLockDelegate {
    func didPatternInput(patterLock: OXPatternLock, track: [Int]) {
        timer?.invalidate()
        if savedTrackPath.isEmpty {
            
            
            //
            savedTrackPath = track
            showStatus(message: "Lütfen bir defa daha giriniz.")
            UserDefaults.standard.set(savedTrackPath, forKey: "lockKey")

        } else {
            if savedTrackPath == track {
                showStatus(message: "Doğru")
                UserDefaults.standard.set(false, forKey: "lock")
                UserDefaults.standard.synchronize()
                performSegue(withIdentifier: "lockToMenu", sender: self)

            } else {
                showStatus(message: "Yanlış")
            }
        }
    }
}


//extension EbeveynViewController: OXPatternLockDelegate {
//    func didPatternInput(patterLock: OXPatternLock, track: [Int]) {
//        timer?.invalidate()
//        if savedTrackPath.isEmpty {
//            savedTrackPath = track
//            showStatus(message: "Kaydedildi")
//
//
////  true kilitli - false kilitsiz
//
//        } else {
//            if savedTrackPath == track {
//                UserDefaults.standard.set(true, forKey: "lock")
//                UserDefaults.standard.synchronize()
//
//                performSegue(withIdentifier: "lockToMenu", sender: self)
//
//                showStatus(message: "Doğru")
//            } else {
//                showStatus(message: "Yanlış")
//            }
//        }
//    }
//}
