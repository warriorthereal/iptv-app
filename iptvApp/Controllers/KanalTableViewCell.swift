//
//  KanalTableViewCell.swift
//  iptvApp
//
//  Created by Arif Doğan on 24.08.2018.
//  Copyright © 2018 Arif Doğan. All rights reserved.
//

import UIKit

class KanalTableViewCell: UITableViewCell {

    @IBOutlet weak var kanalLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
