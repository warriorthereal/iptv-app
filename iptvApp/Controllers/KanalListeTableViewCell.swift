//
//  KanalListeTableViewCell.swift
//  iptvApp
//
//  Created by Arif Doğan on 25.08.2018.
//  Copyright © 2018 Arif Doğan. All rights reserved.
//

import UIKit

class KanalListeTableViewCell: UITableViewCell {

    @IBOutlet weak var kanalLabel: UILabel!
    
    @IBOutlet weak var gorsel: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()


    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
