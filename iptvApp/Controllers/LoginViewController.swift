//
//  LoginViewController.swift
//  iptvApp
//
//  Created by Arif Doğan on 22.08.2018.
//  Copyright © 2018 Arif Doğan. All rights reserved.
//

import UIKit
import SideMenuSwift
class LoginViewController: UIViewController {

    @IBOutlet weak var xstreamImage: UILabel!
    @IBOutlet weak var fileImage: UIImageView!
    @IBOutlet weak var m3uImage: UILabel!
    @IBOutlet weak var sideMenuImage: UIImageView!
    
    @IBOutlet weak var lockImage: UIImageView!
    
    
    var lock = UserDefaults.standard.bool(forKey: "lock")
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.backgroundColor = UIColor(patternImage: UIImage(named: "loginback.png")!)
        
        self.m3uImage.isUserInteractionEnabled = true
        self.xstreamImage.isUserInteractionEnabled = true
        self.fileImage.isUserInteractionEnabled = true
        
        recos()
        
        SideMenuController.preferences.basic.menuWidth = 350
        SideMenuController.preferences.basic.position = .sideBySide
        SideMenuController.preferences.basic.direction = .left
        SideMenuController.preferences.basic.enablePanGesture = true
        
       
        
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if(self.lock == false){
            
            self.lockImage.image = UIImage(named: "lock.png")
            
        }else {
            
            self.lockImage.image = UIImage(named: "openlock.png")
        }
        
        
    }
    
    func recos(){
        
        
        let recom3 = UITapGestureRecognizer(target: self, action: #selector(LoginViewController.tom3))
        
        self.m3uImage.addGestureRecognizer(recom3)
        
        
        let recoside = UITapGestureRecognizer(target: self, action: #selector(LoginViewController.sideMenu))
        self.sideMenuImage.isUserInteractionEnabled = true
        self.sideMenuImage.addGestureRecognizer(recoside)
        
        let lockSegue = UITapGestureRecognizer(target: self, action: #selector(LoginViewController.lockSegue))
        
        self.lockImage.isUserInteractionEnabled = true
        self.lockImage.addGestureRecognizer(lockSegue)
    }
    
    
    @objc func lockSegue(){
        
        performSegue(withIdentifier: "toLock", sender: self)
        
    }
    @objc func sideMenu(){
        print("Clicked")
        
        sideMenuController?.revealMenu()
    }

    @objc func tom3(){
        
        performSegue(withIdentifier: "m3uLogin", sender: nil)
        
    }

}
