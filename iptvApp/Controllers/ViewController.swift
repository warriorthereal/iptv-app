//
//  ViewController.swift
//  iptvApp
//
//  Created by Arif Doğan on 21.08.2018.
//  Copyright © 2018 Arif Doğan. All rights reserved.
//

import UIKit
import SnapKit
import BMPlayer


class ViewController: UIViewController{

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let player = BMPlayer()
        view.addSubview(player)
        player.snp.makeConstraints { (make) in
            make.top.equalTo(self.view).offset(20)
            make.left.right.equalTo(self.view)
            // Note here, the aspect ratio 16:9 priority is lower than 1000 on the line, because the 4S iPhone aspect ratio is not 16:9
            make.height.equalTo(player.snp.width).multipliedBy(9.0/16.0).priority(750)
        }
        // Back button event
        player.backBlock = { [unowned self] (isFullScreen) in
            if isFullScreen == true { return }
            let _ = self.navigationController?.popViewController(animated: true)

        }
        BMPlayerConf.loaderType = .ballPulseRise
        
        
        let asset = BMPlayerResource(url: URL(string: "https://www.hdpvrcapture.com/hdpvrcapture/samples/20090228_085119-H.264.m2ts")!)
        player.setVideo(resource: asset)
    }

}
