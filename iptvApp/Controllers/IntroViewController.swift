//
//  IntroViewController.swift
//  iptvApp
//
//  Created by Arif Doğan on 22.08.2018.
//  Copyright © 2018 Arif Doğan. All rights reserved.
//

import UIKit

class IntroViewController: UIViewController {

    @IBOutlet weak var goImage: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()

        
        self.view.backgroundColor = UIColor(patternImage: UIImage(named: "backgroundIntro.png")!)

        let reco = UITapGestureRecognizer(target: self, action: #selector(IntroViewController.goMenu))
        
        self.goImage.isUserInteractionEnabled = true
        
        self.goImage.addGestureRecognizer(reco)
        // Do any additional setup after loading the view.
    }

    @objc func goMenu(){
        
        performSegue(withIdentifier: "toMenu", sender: nil)
        
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
