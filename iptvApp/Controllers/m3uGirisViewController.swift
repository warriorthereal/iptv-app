//
//  m3uGirisViewController.swift
//  iptvApp
//
//  Created by Arif Doğan on 22.08.2018.
//  Copyright © 2018 Arif Doğan. All rights reserved.
//

import UIKit

class m3uGirisViewController: UIViewController {

    
    enum LINE_POSITION {
        case LINE_POSITION_TOP
        case LINE_POSITION_BOTTOM
    }
    
    func addLineToView(view : UIView, position : LINE_POSITION, color: UIColor, width: Double) {
        let lineView = UIView()
        lineView.backgroundColor = color
        lineView.translatesAutoresizingMaskIntoConstraints = false // This is important!
        view.addSubview(lineView)
        
        let metrics = ["width" : NSNumber(value: width)]
        let views = ["lineView" : lineView]
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[lineView]|", options:NSLayoutFormatOptions(rawValue: 0), metrics:metrics, views:views))
        
        switch position {
        case .LINE_POSITION_TOP:
            view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[lineView(width)]", options:NSLayoutFormatOptions(rawValue: 0), metrics:metrics, views:views))
            break
        case .LINE_POSITION_BOTTOM:
            view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[lineView(width)]|", options:NSLayoutFormatOptions(rawValue: 0), metrics:metrics, views:views))
            break
        default:
            break
        }
    }
    
    @IBOutlet weak var textfield: UITextField!
    @IBOutlet weak var girisImage: UIImageView!
    
    var urlstring : String = ""
    
   
    override func viewDidLoad() {
        super.viewDidLoad()

        
        let tap = UITapGestureRecognizer(target: self, action: #selector(m3uGirisViewController.endediting))
        self.view.addGestureRecognizer(tap)

        self.view.backgroundColor = UIColor(patternImage: UIImage(named: "loginback.png")!)
        
        self.addLineToView(view: self.textfield, position:.LINE_POSITION_BOTTOM, color: UIColor.darkGray, width: 0.5)
        
        
        self.girisImage.isUserInteractionEnabled = true
        
        let reco = UITapGestureRecognizer(target: self, action: #selector(m3uGirisViewController.tom3u))
        
        self.girisImage.addGestureRecognizer(reco)

        
    }
    @objc func endediting (){
        
        self.view.endEditing(true)
        
    }
    
    @objc func tom3u(){
       
        
        if ( self.textfield.text != "") {
            
            
            
            self.urlstring = self.textfield.text as! String
            
            UserDefaults.standard.set(self.urlstring, forKey: "link")
            UserDefaults.standard.synchronize()
            
            performSegue(withIdentifier: "tom3u", sender: nil)
            
        }else {
            
            let alert = UIAlertController(title: "Hata", message: "Lütfen Boş Bırakmayın", preferredStyle: UIAlertControllerStyle.alert)
            
            let okButton = UIAlertAction(title: "Tamam", style: .cancel, handler: nil)
            
            alert.addAction(okButton)
            
            self.present(alert,animated: true,completion: nil)
            
            
            
            
        }
        
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if ( segue.identifier == "tom3u") {
            
            let destination = segue.destination as! KanalListelemeViewController
            
            destination.selectedm3u = self.urlstring as! String
           
    
        }
    }
   
    

}
