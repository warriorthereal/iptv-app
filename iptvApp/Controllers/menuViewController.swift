//
//  menuViewController.swift
//  iptvApp
//
//  Created by Arif Doğan on 26.08.2018.
//  Copyright © 2018 Arif Doğan. All rights reserved.
//

import UIKit

class menuViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    
    var link = UserDefaults.standard.string(forKey: "link")
    
    var menuLabels = ["EBEVEYN KONTROL", "IPTV PLAYER", "PREMIUM", "KANAL LISTESI", "VIDEOLAR", "FAVORI LISTESI", "AYARLAR"]
    var segueList = ["EBEVEYN KONTROL", "", "", "KANAL LISTESI", "", "", ""]
    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.backgroundColor = UIColor(patternImage: UIImage(named: "girlBackground.png")!)
        
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
    }


    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "sideCell", for: indexPath)
            as! sideTableViewCell
        
        cell.backgroundColor = UIColor.clear
        cell.nameLabel.text = self.menuLabels[indexPath.row]
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        if ( self.segueList[indexPath.row] == ""){
            
            
            print("segue error")
            
            let alert = UIAlertController(title: "Yakında", message: "Ozellik cok yakında geliyor.", preferredStyle: UIAlertControllerStyle.alert)
            
            let okButton = UIAlertAction(title: "Tamam", style: UIAlertActionStyle.cancel, handler: nil)
            
            alert.addAction(okButton)
            
            self.present(alert,animated: true,completion: nil)
            
        }
        
        if (self.segueList[indexPath.row] == "KANAL LISTESI"){
            
            if (self.link == nil){
                
                
                
                let alert = UIAlertController(title: "GIRIS", message: "Lutfen Once Giris Yapın.", preferredStyle: UIAlertControllerStyle.alert)
                
                let okButton = UIAlertAction(title: "Tamam", style: UIAlertActionStyle.cancel, handler: nil)
                
                alert.addAction(okButton)
                
                self.present(alert,animated: true,completion: nil)
                
                
                
            } else {
                
                performSegue(withIdentifier: self.menuLabels[indexPath.row], sender: nil)
                
                print("SEGUE")
            }
            
            
            
        }
        
        
        if (self.segueList[indexPath.row] == "EBEVEYN KONTROL"){
            
           
                
                performSegue(withIdentifier: self.menuLabels[indexPath.row], sender: nil)
                
                print("SEGUE")
            
            
            
            
        }
        
        //////
    
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.menuLabels.count
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 55
        }
    }

