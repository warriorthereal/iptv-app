//
//  PlayerViewController.swift
//  iptvApp
//
//  Created by Arif Doğan on 25.08.2018.
//  Copyright © 2018 Arif Doğan. All rights reserved.
//

import UIKit
import BMPlayer
import SnapKit

class PlayerViewController: UIViewController {

    @IBOutlet weak var choosenTitleLabel: UILabel!
    var choosenUrl : String = ""
    var choosenTitle : String = ""
    var url : URL = URL(string: "www.google.com")!
    var choosenTitleString : String = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor(patternImage: UIImage(named: "loginback.png")!)

        
        print(self.choosenUrl)
        print(self.choosenTitle)
        print(self.url)
        addPlayer()
        
        self.choosenTitleLabel.text = self.choosenTitle as! String
    }

    override var prefersStatusBarHidden: Bool{
        return true
    }

   
    func addPlayer(){
        
        
        let player = BMPlayer()
        view.addSubview(player)
        player.snp.makeConstraints { (make) in
            make.top.equalTo(self.view).offset(20)
            make.left.right.equalTo(self.view)
            // Note here, the aspect ratio 16:9 priority is lower than 1000 on the line, because the 4S iPhone aspect ratio is not 16:9
            make.height.equalTo(player.snp.width).multipliedBy(9.0/16.0).priority(750)
        }
        // Back button event
        player.backBlock = { [unowned self] (isFullScreen) in
            if isFullScreen == true { return }
            
            self.performSegue(withIdentifier: "toList", sender: nil)
        
            
        }
        
        
        let asset = BMPlayerResource(url:self.url,
                                     name: "\(self.choosenTitle)")
        player.setVideo(resource: asset)
        
        
        
    }
}
