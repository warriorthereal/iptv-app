//
//  KanalListelemeViewController.swift
//  BMPlayer
//
//  Created by Arif Doğan on 22.08.2018.
//

import UIKit
import Alamofire
import SwiftyJSON
import SideMenuSwift

class Kanal {
    
    var kategori : String = ""
    var kanalAdi : String = ""
    var link : String = ""
    
}


class KanalListelemeViewController: UIViewController , UITableViewDelegate, UITableViewDataSource{

    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    
    var ebeveynKilit = UserDefaults.standard.bool(forKey: "lock")
    
    
    var linkArray = [String]()
    var kategoriArray = [String]()
    var IsimArray = [String]()
    var kategoriTipi = [String]()
    var last : String = ""
    var selectedTitle : String = ""
    var selectedm3u : String = UserDefaults.standard.string(forKey: "link")!
    
    var kanalList = [Kanal]()

    @IBOutlet weak var logoImage: UIImageView!
    
    var selectedURL : String = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.view.backgroundColor = UIColor(patternImage: UIImage(named: "loginback.png")!)


        print(self.selectedm3u)
        
        self.logoImage.isUserInteractionEnabled = true
        
        
        let menuReco = UITapGestureRecognizer(target: self, action: #selector(KanalListelemeViewController.toMenu))
    
        self.logoImage.addGestureRecognizer(menuReco)
    }
    
    @objc func toMenu(){
        
        performSegue(withIdentifier: "toMenu", sender: nil)
    }
  
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "kanalCell", for: indexPath)
            as! KanalListeTableViewCell
        

        cell.gorsel.image = UIImage(named: "tvImage.png")
        cell.backgroundColor = UIColor.clear
        cell.kanalLabel.text = self.IsimArray[indexPath.row]
        
      //  print(self.IsimArray)
        
        
        return cell
    }
    

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.selectedURL = self.linkArray[indexPath.row]
        
        self.selectedTitle = self.IsimArray[indexPath.row]
        
        performSegue(withIdentifier: "toVideo", sender: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if ( segue.identifier == "toVideo"){
            
            let destination = segue.destination as! PlayerViewController
            
            var urlString = self.selectedURL.components(separatedBy: "\r")
            
            print(urlString[0])
            let url : URL = URL(string: "\(urlString[0])")!
            
            destination.url = url
            destination.choosenTitle = self.selectedTitle
            destination.choosenUrl = self.selectedURL
            
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
     //print(self.kategoriArray)
        return self.IsimArray.count
        
    }
    
    override func loadView() {
        super.loadView()
        
       // getData()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.tableView.dataSource = self
        
        self.tableView.delegate = self
        
        
        print("Will Appear")
        
        getData()
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 65
    }
  

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)


      //  print(self.kanalList)
      //  print("ASD")
        for kanal in self.kanalList {

          //  print("ASD")
            
           // print(kanal.kanalAdi)
        //    print(kanal.kategori)
           // print(kanal.link)
        }
    }
    
    func getData(){
        Alamofire.request("\(self.selectedm3u as! String)").response { response in
           
        
            if let data = response.data, let utf8Text = String(data: data, encoding: .utf8) {
                self.last = utf8Text
                self.ParseData()
              // print(self.last)
            }
        }
    }
    
    
    
    func ParseData(){
        var satirlar = last.components(separatedBy: ["\n"])
        
        for satir in satirlar {
            
            var kanal1 : Kanal! = Kanal()
            
            if ( satir.contains("#")){
                
                var kanallar = satir.components(separatedBy: ",")
                
                for kanalKategori in kanallar {
                    
                    if kanalKategori.contains("#"){
                        
                        
                        
                    }else{
                        
                        
                        
                        var kanalkategoriler = kanalKategori.components(separatedBy: ":")
                        
                        
                        //  print(kanalkategoriler)
                        
                        if ( kanalkategoriler.count > 1){
                            
                            
                            // print(kanalkategoriler[0])
                            
                            //    print(kanalkategoriler[1])
                            
                            kanal1.kanalAdi = kanalkategoriler[1]
                            kanal1.kategori = kanalkategoriler[0]
                        }else {
                            
                            
                            kanal1.kanalAdi = kanalkategoriler[0]
                            
                            
                        }
                        
                        
                    }
                    
                }
            }else {
                
                //  print(satir)
                kanal1.link = satir
                
                
            }
            
            self.kanalList.append(kanal1)
            
            
            
            
           
            
            self.kategoriArray.append(kanal1.kategori)
            
            self.IsimArray.append(kanal1.kanalAdi)
            
            self.linkArray.append(kanal1.link)
            
            
            
       
        }
        
        
        self.IsimArray = self.IsimArray.filter({ $0 != ""})
        
        self.kategoriArray = self.kategoriArray.filter({ $0 != ""})
        
        
        
        self.linkArray = self.linkArray.filter({ $0 != ""})
        
        
      print(self.kategoriArray)
        
        self.tableView.reloadData()
    }
    
    
    

    @IBAction func reload(_ sender: Any) {
        
        getData()
        self.tableView.reloadData()
    }
}
